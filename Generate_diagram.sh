#!/usr/bin/env bash

##
## Set default values:
##

# map filename to quarter:
export currentMonth=$(date "+%h")
export currentYear=$(date "+%Y")
export currentDay=$(date "+ %m-%d")
export currentQuarter=$(( ($(date "+%m")-1)/3 +1 ))

# Reading script dir - and set up folder for the temoprary files:
export scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export tempDir="${scriptDir}/temp"
export templateFolder="${scriptDir}/templates"

# Cleaning up temporary folder:
mkdir -p "${tempDir}"
if [[ ! -z $(ls "${tempDir}") ]];then rm ${tempDir}/*; fi

# Essential requirements. Will be tested in as a first step:
# Any of these essential parameters will fail, the script exits.
export essentialTools=("convert")
export essentialFiles=("Verdana.ttf"
    "EMBL_EBI_Logo_black_x1000.png"
    "NHGRI_Full_color.png"
    "diagram_legend.png")
##
## Functions
##

# This function is called to test the existence of the template files that the script relies on:
function CheckEssentials (){
    # As these requirements are all considered as essential, if any of them fails, the script fails with exit code 9

    # Testing the existence of the template directory:
    if [[ ! -d "${templateFolder}" ]]; then sendMail "[Error] Template folder $templateFolder does not exists. Exiting." 9; fi

    # Testing tools:
    for tool in "${essentialTools[@]}"; do
        if [[ -z $(which ${tool}) ]]; then sendMail "[Error] $tool is not in the path. Essential requirement. Exiting." 9;  fi
    done

    # Testing files:
    for file in "${essentialFiles[@]}"; do
        if [[ ! -e "${templateFolder}/${file}" ]]; then sendMail "[Error] ${file} is not in the template folder. Essential requirement. Exiting." 9;  fi
    done
}

# General help function:
function display_help (){

    # There could be some error/warning messages that we also want to print out:
    if [[ ! -z ${1} ]]; then echo "${1}"; fi

    # Now print out the actual help:
    echo "This script is part of the GWAS catalog diagram creator bamboo plan.

This script downloads the most recent gwas catalog diagram, fixes the svg file, and converts it
to pdf and png. Then moves to its final destination. (Will later moved into the ftp directory)

Usage:
    $0 -t <target_directory> -s <server_address> -r <horizontal_res> -e <email_list> -l <log_dir> -x

    -t <target_directory>  - The output files will be copied into this folder. There should be three subfolders: png, svg, pdf
    -s <server_address>    - Server where the diagram is created (eg. http://snoopy.ebi.ac.uk:8080/gwas/pussycat/gwasdiagram/associations).
    -r <horizontal_res>    - Horizontal resolution of the converted image (integer).
    -e <email_list>        - Comma separated list of emails of the recipients
    -l <log_dir>           - Folder in which the logs will be stored
    -h                     - Print out this help message and exit.

All the above listed options are required.

As the script is designed to automatic execution, there is a list of exit codes:
    0 - Everything went well, successful execution.
    1 - Email list is not specified or not good.
    2 - Log directory is not specified or wrong.
    3 - Target directory is not specified or wrong.
    4 - Server address is not specified.
    5 - Resolution is not specified or format not good
    6 - Script is run outside of the quarterly manner. Use -x flag.
    7 - ImageMagick is not installed.
    8 - Curling svg file from Pussycat has been failed.
    9 - Image conversion has been failed.
    10 - Copying images to target directory has been failed.
    11 - Generating the annotated slide has been failed.

If anything goes wrong, email me: dsuveges@ebi.ac.uk
Last modified: 2018.05.09"

    exit;
}

# Final function. Anything fails, it will be called and emails will be sent.
function sendMail(){
    # $logFile is exported from the main.

    message="$1"
    exitCode="$2"
    subject="[Generage diagram report] "

    # We adjust the subject of the message based on the exit code:
    if [[ ${exitCode} == 0 ]]; then
        subject="${subject} Script successfully finished."
    else
        subject="${subject} Script has failed."
    fi

    # Compiling and sending mail:
    cat <(echo -e "${message}\n\nGenerated log file:\n") ${logFile}| mutt -s "${subject}" "${emailList}"
    exit ${exitCode};
}

# The only argument this function takes is the input file name
function generateAnnotatedSlide (){

    # the previously generated png as input, won't be tested here.
    pngFile="${1}"

    # All these parameters are built in. We don't expect them to change.
    resolution="2500x2000"

    # Annotation:
    summaryTextBottom="NHGRI-EBI GWAS Catalog\nwww.ebi.ac.uk/gwas"
    summaryTextTop1="Published Genome-Wide Associations as of ${currentMonth} ${currentYear}" #
    summaryTextTop2="p≤5X10-8 for 17 trait categories"

    # templates added to the slide:
    EBILogo="${scriptDir}/templates/EMBL_EBI_Logo_black_x1000.png"
    NHGRILogo="${scriptDir}/templates/NHGRI_Full_color.png"
    diagramLegend="${scriptDir}/templates/diagram_legend.png"

    # Font loaded from the script directory:
    font="${scriptDir}/templates/Verdana.ttf"

    echo "$currentYear $currentMonth $tempDir $pngFile"

    # This command generates the annotated gwas catalog slide:
    convert -size ${resolution} xc:white \
              "${pngFile}" -geometry 1800x1500+142+264 -composite \
              "${EBILogo}" -geometry 486x150+888+1783 -composite \
              "${NHGRILogo}" -geometry 722x150+72+1783 -composite \
              "${diagramLegend}" -geometry 1062x533+2022+861 -composite \
              -pointsize 75 -font "${font}" -gravity north -annotate +0+50 "${summaryTextTop1}" \
              -pointsize 55 -font "${font}" -gravity north -annotate +0+150 "${summaryTextTop2}" \
              -size 790x200 -background transparent -pointsize 65 -font "${font}" -gravity south -annotate +700+50 "${summaryTextBottom}" \
            "${tempDir}/current_annotated_diagram.pdf"

    # Check the output of imagemagick:
    if [[ -e  "${tempDir}/current_annotated_diagram.pdf" ]]; then
        return 0;
    else
        return 1;
    fi
}

##
## Processing command line options:
##

# Accepting command line parameters:
OPTIND=1
while getopts "h?t:s:r:l:e:" opt; do
    case "$opt" in
        "t" ) targetDir=$( readlink -e "${OPTARG}" ) ;; # Target directory in which the files will be copyied at the end
        "s" ) serverAddress="${OPTARG}" ;; # Address where pussycat is running
        "r" ) resolution="${OPTARG}" ;; # Horizontal resolution of the png image.
        "l" ) logDir=$( readlink -e "${OPTARG}" ) ;; # Directory where the logs will be created.
        "e" ) export emailList="${OPTARG}" ;; # List of the email addresses of the recipients.
        "h" | *  ) display_help ;;
     esac
done

##
## Check all command line parameters. If anything is wrong, the script dies.
##

# Checking essential requirements:
CheckEssentials

# Check email list:
if [[ -z ${emailList} ]]; then
    echo "[Error] Email list is not specified! Exiting." >&2
    exit 1;
elif [[ -z $(echo $emailList | perl -lane 'print "OK" if $_ =~ /\S@\S+\.\S+/' ) ]]; then
    echo "[Error] The scpecified email list (${emailList}) does not contain email address. Exiting." >&2
    exit 1;
fi

# Check log directory:
if [[ -z ${logDir} ]]; then
    sendMail "[Error] Directory of the logs is not specified! Exiting." 2
elif [[ ! -d ${logDir} ]]; then
    sendMail "[Error] Log directory (${logDir}) does not exist. Exiting." 2
fi

# Check target directory:
if [[ -z ${targetDir} ]]; then
    sendMail "[Error] Targetdir is not specified! Exiting." 3
elif [[ ! -d ${targetDir} ]]; then
    sendMail "[Error] Target directory (${targetDir}) does not exist. Exiting." 3
fi

# Check server address:
if [[ -z ${serverAddress} ]]; then
    sendMail "[Error] Server address is not specified! Exiting." 4
fi

# Check resolution:
if [[ -z ${resolution} ]]; then
    sendMail "[Error] Resolution is not specified! Exiting." 5
elif [[ -z $( echo ${resolution} | perl -lane 'print "OK" if $_ =~ /^[0-9]+$/' ) ]]; then
    sendMail "[Error] Format of resolution (${resolution}) is not as expected, a single integer is accepted. Exiting." 5
fi

##
## Check derived parameters:
##

# The script exits if imagemagick is not installed:
if [[ -z $(which convert) ]]; then
    sendMail "[Error] ImageMagick is not installed on this system. Download from here: http://www.imagemagick.org/" 7;
fi;

# Extract ImageMagick version:
IM_version=$(convert --version | grep -i version | perl -lane 'print $_ =~ /ImageMagick (\S+) / ? $1 : NA')

# Echo reports.
today=$(date "+%Y.%m.%d")
export logFile="${logDir}/${today}_generate_diagram.log"
echo "[Info] __Quarterly diagram generation__

[Info] Script: $0
[Info] Run date: ${today}, Q${currentQuarter}
[Info] Log file: $logFile
[Info] Target directory: $targetDir
[Info] Server address: $serverAddress
[Info] p-value cutoff: 5e-8
[Info] Image horizontal resolution: $resolution
[Info] Report is sent to the following email addresses: $emailList
[Info] ImageMagick version: $IM_version (Tested on: 6.7.8-9)" > "${logFile}"

# Cleaning up current folder:
rm -rf ${targetDir}/current

##
## Step 1: Call pussycat call to extract the diagram.
##

# Calling pussycat:
echo -e "\n[Info] Fetching svg from pussycat server..." >> "${logFile}"
curl -s --max-time 3600 --connect-timeout 3600 --retry 5 -f ${serverAddress}?pvaluemax=5e-8 > ${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}_tofix.svg

# Checking if curl was successful:
if [[ ! -e ${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}_tofix.svg ]]; then
    sendMail "[Error] Retrieving svg from the server has been failed. Exiting" 8
elif [[ -z $(grep svg ${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}_tofix.svg ) ]]; then
    sendMail "[Error] Although the svg file is generated, it's wrong. Exiting" 8
else
    echo "[Info] The svg file has been successfully returned: ${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}_tofix.svg" >> "${logFile}"
fi

##
## Step 2: Fixing the svg file (as this is just a simple perl run, I don't test.)
##

cat "${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}_tofix.svg" \
    | perl -lane '$_ =~ s/x\=\"(\d+\.\d+)\s+\d+\.\d+/x=\"$1/g if $_ =~ /label/; print $_' \
    > "${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}.svg";
echo "[Info] The svg file successfully adjusted." >> "${logFile}"

##
## Step 3: Converting svg files into png
##

convert -background none -density 300 -resize "${resolution}x" "${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}.svg"  "${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}.png"
if [[ ! $? ]]; then sendMail "[Error] The svg file could not be successfully converted to png. Exiting." 9; else echo "[Info] png file successfully created." >> "${logFile}"; fi

convert -background none -density 300 -resize "${resolution}x" "${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}.svg"  "${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}.pdf"
if [[ ! $? ]]; then sendMail "[Error] The svg file could not be successfully converted to pdf. Exiting." 9; else echo "[Info] pdf file successfully created." >> "${logFile}"; fi

##
## Step 4: Copying svg, png and pdf files to their final location
##

# We make sure the folders for each filetype, are there:
mkdir -p "${targetDir}/svg"
mkdir -p "${targetDir}/png"
mkdir -p "${targetDir}/pdf"
mkdir -p "${targetDir}/current"

# Let's copy:
cp "${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}.png" "${targetDir}/png"
cp "${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}.png" "${targetDir}/current/diagram.png"
if [[ ! $? ]]; then
    sendMail "[Error] The png file could not be copied to the target directory. Exiting." 10;
else
    echo "[Info] png file successfully copied to the target directory." >> "${logFile}";
fi

cp "${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}.svg" "${targetDir}/svg"
cp "${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}.svg" "${targetDir}/current/diagram.svg"
if [[ ! $? ]]; then
    sendMail "[Error] The svg file could not be copied to the target directory. Exiting." 10;
else
    echo "[Info] svg file successfully copied to the target directory." >> "${logFile}";
fi

cp "${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}.pdf" "${targetDir}/pdf"
cp "${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}.pdf" "${targetDir}/current/diagram.pdf"
if [[ ! $? ]]; then
    sendMail "[Error] The pdf file could not be copied to the target directory. Exiting." 10;
else
    echo "[Info] pdf file successfully copied to the target directory." >> "${logFile}";
fi

##
## Generate summary pdf-s with all the stuffs:
##
echo "[Info] Generating summary slide." >> "${logFile}";
generateAnnotatedSlide "${tempDir}/gwas-diagram_${currentYear}-${currentQuarter}.png"

if [[ ! $? ]]; then
    sendMail "[Error] The annotated slide could not be generated. Exiting." 11;
else
    echo "[Info] The annotated slide is successfully created." >> "${logFile}";
fi
cp "${tempDir}/current_annotated_diagram.pdf" "${targetDir}/current/"

## It seems the script has not failed anywhere, so success.
sendMail "[Info] The script has successfully finished. Exiting." 0;

