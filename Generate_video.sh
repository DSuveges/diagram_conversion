#!/usr/bin/env bash

# This script is part of a bamboo plan to generate gwas diagrams and videos quarterly.
# More scpecifically this script combines diagrams into videos.

# I have decided to generate the whole animation from scretch every time,
# As if we decide to modify something, there whould be no porblem with the update

# Workflow:
# 1. Download the most recent association and study files.
# 2. Generate opening slides.
# 3. Annotating diagram png files.
# 4. Compiling all diagrams together.
# 5. Compiling the opening and the diagram parts of the video together
# 6. Convert videos to ogg, mp4 and webm formats.
# 7. Copy videos to their final location.
# 8. Clean up, send report to recipients.


##
## Common variables
##
# map filename to quarter:
export today=$(date "+%Y-%m-%d")

# Reading script dir - and set up folder for the temoprary files:
export scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export tempDir="${scriptDir}/temp"

# Cleaning up temporary folder:
mkdir -p "${tempDir}"
if [[ ! -z $(ls "${tempDir}") ]];then rm -rf ${tempDir}/*; fi

# GWAS Catalog green:
export slideBackgroundColor='#E2F6F8'
export linkTextColor='#6495ED' # Cornflowerblue

# Template folder:
export templateFolder="${scriptDir}/templates"

# Slide dimension:
export slideResolution="1800x1013"

# Essential requirements. Will be tested in as a first step:
# Any of these essential parameters will fail, the script exits.
export essentialTools=("ffmpeg" "convert")
export essentialFiles=("VerdanaBold.ttf"
    "Verdana.ttf"
    "GWAS_Catalog_logo_240x240.png"
    "gwas_slide_logo.png")

# Video parameters:
ffmpeg_sec=25.7 # Constant to stretch the video; so one frame become exactly 1 sec.
first_slide_sec=2 # How long the first slide will be shown
second_slide_sec=5 # How long the second slide will be shown
diagram_sec=0.3 # How long each diagrams will be shown.
ffmpeg_param=$(echo "${diagram_sec} * ${ffmpeg_sec}" | bc -l )

##
## Functions
##

# Generation of the first slide is completely automatic, no input is required, all variables are imported from main:
function GenerateFirstSlide (){

    # Text added to the slide:
    titleText='GWAS Catalog'
    describeText='The NHGRI-EBI Catalog of published genome-wide\nassociation studies provides a publicly available\ncurated resource of all published GWAS and\nassociation results'
    linkText='www.ebi.ac.uk/gwas'
    referenceText='MacArthur J, et al. The new NHGRI-EBI Catalog of published\ngenome-wide association studies (GWAS Catalog).\nNucleic Acids Research, 2017, Vol.45 (Database issue): D896-D901.\nPMID: 27899670'

    gwasLogo="${templateFolder}/gwas_slide_logo.png"

    # This command generates the annotated gwas catalog slide:
    convert -size ${slideResolution} xc:"${slideBackgroundColor}" \
          "${gwasLogo}" -geometry 422x422+86+350 -composite \
          -pointsize 90 -font "${templateFolder}/VerdanaBold.ttf" -gravity northwest -annotate +600+50 "${titleText}" \
          -size 790x200 -pointsize 45 -font "${templateFolder}/Verdana.ttf" -gravity northwest -annotate +600+300 "${describeText}" \
          -size 790x200 -pointsize 25 -font "${templateFolder}/Verdana.ttf" -gravity northwest -annotate +600+700 "${referenceText}" \
          -size 790x200 -pointsize 45 -font "${templateFolder}/Verdana.ttf" -stroke "${linkTextColor}" -fill "${linkTextColor}" -gravity northwest -annotate +600+575 "${linkText}" \
            "${tempDir}/Slide_1.png"

    # Testing if the command was successful or not?
    if [[ $? ]]; then echo 0; else echo 1; fi
};

function GenerateSecondSlide (){
    assocCount="$1"
    studyCount="$2"
    paperCount="$3"

    # Get current date:
    currentMonth=$(date "+%h")
    currentYear=$(date "+%Y")

    # Texts to be included in the slide:
    titleText='GWAS Catalog'
    text1="As of ${currentMonth} ${currentYear} the GWAS Catalog contains\nmore than $(( ${assocCount//,/} / 1000 )),000 SNP-trait associations and\nover $(( ${studyCount//,/} / 1000 )),000 studies from ${paperCount} publications\n\nThis video shows how the number of associations\nhas increased over this time\n\nEach dot represents a genomic region associated\nwith a trait (p<5e-8), with different colours\nrepresenting trait categories"

    # The only imagecomponent to be added:
    gwasLogo="${templateFolder}/gwas_slide_logo.png"

    # Generating the image:
    convert -size ${slideResolution} xc:"${slideBackgroundColor}" \
            "${gwasLogo}" -geometry 422x422+86+350 -composite \
          -pointsize 90 -font "${templateFolder}/VerdanaBold.ttf" -gravity northwest -annotate +600+50 "${titleText}" \
          -size 790x200 -pointsize 45 -font "${templateFolder}/Verdana.ttf" -gravity northwest -annotate +600+300 "${text1}" \
            "${tempDir}/Slide_2.png"

    # Testing if the command was successful or not?
    if [[ $? ]]; then echo 0; else echo 1; fi
}

# This function returns with the number of associations and the number of studies in the current gwas catalog
function GetAssociationCounts () {

    # If the function runs for the first time, it downloads relevant sources:
    if [[ ! -e "${tempDir}/GWAS_catalog_associations.tsv" ]]; then
        wget -q https://www.ebi.ac.uk/gwas/api/search/downloads/full -O  "${tempDir}/GWAS_catalog_associations.tsv"
        wget -q https://www.ebi.ac.uk/gwas/api/search/downloads/studies -O  "${tempDir}/GWAS_catalog_studies.tsv"
    fi

    # If there's no date submitted, then we take the most recent day:
    dateString=${1:-$(date "+%Y%m%d")}

    # Extracting relevant data:
    assocCount=$(tail -n+2 "${tempDir}/GWAS_catalog_associations.tsv" | awk -v targetDate="${dateString}" 'BEGIN{FS="\t"}{gsub(/-/,"", $4); if(targetDate > $4){print $0}}'| wc -l | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')
    studyCount=$(tail -n+2 "${tempDir}/GWAS_catalog_studies.tsv" | awk -v targetDate="${dateString}" 'BEGIN{FS="\t"}{gsub(/-/,"", $4); if(targetDate > $4){print $0}}'| wc -l | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')
    paperCount=$(tail -n+2 "${tempDir}/GWAS_catalog_studies.tsv" | awk -v targetDate="${dateString}" 'BEGIN{FS="\t"}{gsub(/-/,"", $4); if(targetDate > $4){print $2}}'|  sort -u | wc -l | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')

    # returning data:
    echo ${assocCount:-NA} ${studyCount:-NA} ${paperCount:-NA}
}

# This function is called to test the existence of the template files that the script relies on:
function CheckEssentials (){
    # As these requirements are all considered as essential, if any of them fails, the script fails with exit code 9

    # Testing the existence of the template directory:
    if [[ ! -d "${templateFolder}" ]]; then sendMail "[Error] Template folder $templateFolder does not exists. Exiting." 9; fi

    # Testing tools:
    for tool in "${essentialTools[@]}"; do
        if [[ -z $(which ${tool}) ]]; then sendMail "[Error] $tool is not in the path. Essential requirement. Exiting." 9;  fi
    done

    # Testing files:
    for file in "${essentialFiles[@]}"; do
        if [[ ! -e "${templateFolder}/${file}" ]]; then sendMail "[Error] ${file} is not in the template folder. Essential requirement. Exiting." 9;  fi
    done
}

# General help function:
function display_help (){

    # There could be some error/warning messages that we also want to print out:
    if [[ ! -z ${1} ]]; then echo "${1}"; fi

    # Now print out the actual help:
    echo "This script is part of the GWAS catalog diagram creator bamboo plan.

Generates the quarterly diagram movie.

Usage:
    $0 -t <target_directory> -e <email_list> -l <log_dir> -s <source_directory>

    -t <target_directory>  - The output files will be copied into this folder.
    -s <source_directory>  - Folder with the list of the quarterly generated diagrams (png files, following specific format)
    -e <email_list>        - Comma separated list of emails of the recipients
    -l <log_dir>           - Folder in which the logs will be stored
    -h                     - Print out this help message and exit.

All the above listed options are required.

As the script is designed to automatic execution, there is a list of exit codes:
    0 - Everything went well, successful execution.
    1 - Email is missing or no proper email address has been submitted.
    2 - Log directory has not been specified or does not exists.
    3 - Target directory is not specified or does not exists.
    4 - Source directory is not specified or does not exists.
    5 - Source directory does not contain any png files.
    6 - ffmpeg was not found in the path.
    7 - Generation of the opening slided failed somewhere.
    8 - Error in retrieving study/association/paper counts.
    9 - Some of the essential requirements not met.
   10 - Failed to generate temporary mp4 file from the opening slides.
   11 - Failed to generate temporary mp4 file from the diagrams.
   12 - Failed to merge opening and diagram files.
   13 - Failed to create the final video.
   14 - Failed to generate the slideshow pdf.

If anything goes wrong, email me: dsuveges@ebi.ac.uk
Last modified: 2018.05.11"

    exit;
}

# Sending reports to the defined email list:
function sendMail(){
    # $logFile is exported from the main.

    message="$1"
    exitCode="$2"
    subject="[Generage video report] "

    # We adjust the subject of the message based on the exit code:
    if [[ ${exitCode} == 0 ]]; then
        subject="${subject} Script successfully finished."
    else
        subject="${subject} Script has failed."
    fi

    # Compiling and sending mail:
    cat <(echo -e "${message}\n\nGenerated log file:\n") ${logFile}| mutt -s "${subject}" "${emailList}"
    exit ${exitCode};
}

##
## Check all command line parameters. If anything is wrong, the script dies and reports are sent.
##

# Accepting command line parameters:
OPTIND=1
while getopts "h?t:s:l:e:" opt; do
    case "$opt" in
        "t" ) targetDir=$(readlink -e "${OPTARG}" ) ;;#
        "s" ) source_directory=$(readlink -e "${OPTARG}" ) ;;#
        "l" ) logDir=$(readlink -e "${OPTARG}" ) ;;#
        "e" ) emailList="${OPTARG}" ;;#
        "h" | *  ) display_help ;;
     esac
done

# Check email list:
if [[ -z ${emailList} ]]; then
    echo "[Error] Email list is not specified! Exiting." >&2
    exit 1;
elif [[ -z $(echo $emailList | perl -lane 'print "OK" if $_ =~ /\S@\S+\.\S+/' ) ]]; then
    echo "[Error] The scpecified email list (${emailList}) does not contain email address. Exiting." >&2
    exit 1;
fi

# Checking essential requirements:
CheckEssentials

# Check log directory:
if [[ -z ${logDir} ]]; then
    sendMail "[Error] Directory of the logs is not specified! Exiting." 2
elif [[ ! -d ${logDir} ]]; then
    sendMail "[Error] Log directory (${logDir}) does not exist. Exiting." 2
fi

# Check target directory:
if [[ -z ${targetDir} ]]; then
    sendMail "[Error] Targetdir is not specified! Exiting." 3
elif [[ ! -d ${targetDir} ]]; then
    sendMail "[Error] Target directory (${targetDir}) does not exist. Exiting." 3
fi

# Check source directory:
if [[ -z ${source_directory} ]]; then
    sendMail "[Error] Source directory is not specified! Exiting." 4
elif [[ ! -d ${source_directory} ]]; then
    sendMail "[Error] Source directory (${source_directory}) does not exist. Exiting." 4
fi

# Check if the source directory has any png files: (at this point we don't test if the files are named properly or not. )
if [[ -z $(ls ${source_directory}/*png ) ]]; then
    sendMail "[Error] Source directory ($source_directory) does not contain any png files. Exiting." 5
fi

# Before doing anything the ffmpeg version has to be cheked:
if [[ -z $(which ffmpeg) ]]; then sendMail "[Error] ffmpeg was not found in path. Exiting." 6; fi
ffmpeg_version=$(ffmpeg -version | head -n1 | perl -lane '$_ =~ /(\d.+?) /; print $1')

# Print out report:
today=$(date "+%Y.%m.%d")
export logFile="${logDir}/${today}_generate_video.log"
echo "[Info] __Quarterly diagram generation__

[Info] Script: $0
[Info] Run date: ${today}
[Info] Log file: $logFile
[Info] Target directory: $targetDir
[Info] Source directory from which the png files will be read: $source_directory
[Info] Video resolution: $slideResolution
[Info] Report is sent to the following email addresses: $emailList
[Info] ffmpeg version: $ffmpeg_version (Tested on: 3.4.2-static)

" > "${logFile}"

# Add a warning to the logfile:
echo "[Warning] Although the souce directory contains png files, there's no check if the files follow the required format!!! Continuing." >> ${logFile}

##
## Generating the opening slides
##

# For the second slide we need to generate some numbers:
read assocCount studyCount paperCount <<<$(GetAssociationCounts )

# We have to check if these counts are OK:
if [[ $assocCount == "NA" || $studyCount == "NA" || $paperCount == "NA" ]]; then
    sendMail "[Error] There was an error in retrieving the association count (${assocCount}), study count (${studyCount}) or paper count (${paperCount}). Exiting." 8
else
    echo "[Info] as of ${today} in the GWAS catalog there are $studyCount studies and $assocCount associations from $paperCount papers." >> "${logFile}"
fi

# The first slide is constant, however, I decided to generate it all the time.
if [[ $(GenerateFirstSlide) == 0 ]]; then
    echo "[Info] First slide has been successfully generated." >> "${logFile}"
else
    sendMail "[Error] Generation of the first slide failed. Exiting" 7
fi

# Now call the generator of the second slide with these values
if [[ $( GenerateSecondSlide ${assocCount} ${studyCount} ${paperCount} ) == 0 ]]; then
    echo "[Info] Second slide was successfully generated." >> "${logFile}"
else
    sendMail "[Error] Generation of the second slide failed. Exiting." 7
fi

##
## Annotating the previously generated diagram png files:
##

# Looping through the source folder, read all the files, and create pngs:
read firstYear firstQuarter lastYear lastQuarter <<<$(ls ${source_directory}/*png| perl -lane 'next unless $_ =~ /gwas-diagram_(\d+)-Q(\d)/;
    $h{$1}{$2} = 1;
    END{
        @years = sort {$a <=> $b} keys %h;
        $firstYear = shift @years;
        $lastYear = pop @years;
        @sortedLastQuarters = sort {$a <=> $b} keys %{$h{$lastYear}};
        @sortedFirstQuarters = sort {$a <=> $b} keys %{$h{$firstYear}};
        printf "%s %s %s %s", $firstYear, shift(@sortedFirstQuarters), $lastYear, pop(@sortedLastQuarters)}')

echo "[Info] Processing diagram files...
[Info] First timepoint: ${firstYear}-Q${firstQuarter}, Last timepoint: ${lastYear}-Q${lstQuarter} " >> "${logFile}"

# Looping through all the years and quarters:
for year in $(seq ${firstYear} ${lastYear}); do
    for quarter in {1..4}; do
        pngFile=$(ls /homes/dsuveges/Project/GOCI-2119_GWAS_diagram_quarterly_generation/to_ftp/png/*png | grep gwas-diagram_${year}-Q${quarter})

        if [[ -z "${pngFile}" || ! -e "${pngFile}" ]]; then
            echo "[Warning] $pngFile is missing! File skipped." >> "${logFile}"
            continue
        fi

        # Mark the progression in the logfile:
        echo -n "[Info] Processing $pngFile" >> "${logFile}"

        # Get date in all formats:
        read datestring plotYear month <<<$( echo $pngFile | perl -lane '($year, $quarter) = $_ =~ /gwas-diagram_(\d+)-Q(\d)_fixed.png/; @number = qw(0 04 07 10 01); @name = qw(0 Apr July Oct Jan); $year ++ if $quarter == 4; printf "%s%s01 %s %s", $year, $number[$quarter], $year, $name[$quarter]')

        # Get counts:
        read assocCount studyCount paperCount <<<$(GetAssociationCounts ${datestring} )

        # Generate text:
        upperText="${plotYear} ${month}"
        countsText="Associations: ${assocCount}\n\nStudies: ${studyCount}\n\nPapers: ${paperCount}"
        bottomText="www.ebi.ac.uk/gwas"

        # Generating the slide:
        convert -size ${slideResolution} xc:"${slideBackgroundColor}" \
             "${pngFile}" -geometry 1014x1072+576+0 -composite \
             "${templateFolder}/GWAS_Catalog_logo_240x240.png"  -geometry 89x89+22+887 -composite \
             -pointsize 70 -font "${templateFolder}/VerdanaBold.ttf" -gravity northwest -annotate +80+50 "${upperText}" \
             -pointsize 40 -font "${templateFolder}/Verdana.ttf" -gravity northwest -annotate +80+300 "${countsText}" \
             -pointsize 30 -font "${templateFolder}/Verdana.ttf" -gravity southwest -annotate +130+60 "${bottomText}" \
             "${tempDir}/diagram_slide_${year}_Q${quarter}.png"

        # Checking if the annotation of the diagram was successful or not:
        if [[ ! $? ]]; then
            echo -e "\n[Warning] Annotating $pngFile has failed. Skipping." >> "${logFile}"
        else
            echo ".. OK." >> "${logFile}"
        fi
    done
done

##
## Hopefully everything went well and we can concatenate all the slides into a video:
##

# Pooling opening slides together:
echo "[Info] Pooling opening slides together into a temporary mp4 file." >> "${logFile}"
cat <(yes "${tempDir}/Slide_1.png" | head -n ${first_slide_sec} ) \
    <(yes "${tempDir}/Slide_2.png" | head -n ${second_slide_sec} ) | xargs cat | ffmpeg-10bit -f image2pipe -i - -framerate 1  -filter:v "setpts=${ffmpeg_sec}*PTS" "${tempDir}/opening_slides.mp4"

if [[ $? != 0 ]]; then sendMail "[Error] Video creation of the opening slides failed. Exiting." 10; fi

# Pooling diagrams togetner into an mp4
echo "[Info] Pooling diagram slides together into a temporary mp4 file." >> "${logFile}"
for year in $(seq ${firstYear} ${lastYear}); do
    for quarter in {1..4};do
        pngFile="${tempDir}/diagram_slide_${year}_Q${quarter}.png"


        # Reporting missing snapshots:
        if [[ ! -e ${pngFile} ]]; then
            echo "[Warning] ${pngFile} is missing! File skipped." >> "${logFile}"
            continue;
        fi

        # If we know that the we are processig the last bit, it will be added three times and then break from the loop:
        if [[ "${year}-Q${quarter}" == "${lastYear}-Q${lastQuarter}" ]]; then
            yes ${pngFile} | head -n5
            break
        else
            echo ${pngFile}
        fi
    done
done | xargs cat | ffmpeg-10bit -f image2pipe -i - -framerate 1  -filter:v "setpts=${ffmpeg_param}*PTS" "${tempDir}/gwas_slides.mp4"
if [[ ! -e "${tempDir}/gwas_slides.mp4" ]]; then sendMail "[Error] Video creation of the diagrams failed. Exiting." 11; fi

# Merge the opening slides and the diagram slides:
for f in "${tempDir}/opening_slides.mp4" "${tempDir}/gwas_slides.mp4"; do
    echo "file '$f'";
done > "${tempDir}/temp_filelist.txt"

# Calling ffmpeg for the compilation:
echo "[Info] Merging temporary mp4 files." >> "${logFile}"
ffmpeg -f concat -safe 0 -i "${tempDir}/temp_filelist.txt" -c copy "${tempDir}/temp_compiled.mp4"
if [[ ! -e "${tempDir}/temp_compiled.mp4" ]]; then sendMail "[Error] Failed to merge opening and diagram files. Exiting." 12; fi

# Do the suggested modification:
echo "[Info] Creating other formats of the video." >> "${logFile}"
ffmpeg  -i "${tempDir}/temp_compiled.mp4" -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" -pix_fmt yuv420p "${tempDir}/video.mp4" "${tempDir}/video.webm" "${tempDir}/video.ogg"
if [[ ! -e "${tempDir}/video.mp4" ]]; then sendMail "[Error] Failed to create final videos" 14; fi

# Creating a slideshow of the opening slides and diagrams:
echo "[Info] Creating slideshow in pdf format." >> "${logFile}"
echo -e "${tempDir}/Slide_1.png\n${tempDir}/Slide_2.png" > "${tempDir}/fileList.txt"
for year in $(seq ${firstYear} ${lastYear}); do
    for quarter in {1..4}; do
        pngFile="${tempDir}/diagram_slide_${year}_Q${quarter}.png"
        if [[ -e "${pngFile}" ]]; then echo "${pngFile}"; fi
    done
done >> "${tempDir}/fileList.txt"

convert @"${tempDir}/fileList.txt" "${tempDir}/slideshow.pdf"
if [[ ! -e "${tempDir}/slideshow.pdf" ]]; then sendMail "[Error] Slideshow could not be generated. Exiting." 15; fi

# moving files to their final location:
cp "${tempDir}/video.ogg" "${targetDir}"
cp "${tempDir}/video.webm" "${targetDir}"
cp "${tempDir}/video.mp4" "${targetDir}"
cp "${tempDir}/slideshow.pdf" "${targetDir}"

# If everything went fine, we cleaning up and send report:
rm -rf ${tempDir}/*

sendMail "The script has successfully finished." 0
